import PIL
from PIL import Image, ImageEnhance
import numpy as np
import os
import json
import random
import shutil
import cv2
import matplotlib.pyplot as plt


def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)


def rotate(image, points, ang):
	img = image.copy()
	H, W, _ = img.shape
	cx, cy = W/2, H/2
	# rdeg = random.randint(10, 30) * fact
	rdeg = ang
	M = cv2.getRotationMatrix2D((cx, cy), rdeg, 1.0)
	c = np.abs(M[0, 0])
	s = np.abs(M[0, 1])
	nW = int((H*s) + (W*c))
	nH = int((H*c) + (W*s))
	M[0, 2] += ((nW/2) - cx)
	M[1, 2] += ((nH/2) - cy)
	rotated = cv2.warpAffine(img, M, (nW, nH))
	rot_box = []
	# for pts in points:
	# pts = points[p]
	n1 = np.dot([points[0][0], points[0][1], 1], M.T)
	n2 = np.dot([points[1][0], points[1][1], 1], M.T)
	# n3 = np.dot([pts[2][0], pts[2][1], 1], M.T)
	# n4 = np.dot([pts[3][0], pts[3][1], 1], M.T)
	# n5 = np.dot([pts[4][0], pts[4][1], 1], M.T)
	rot_box = [list(n1[:2]), list(n2[:2])]#, list(n3[:2]), list(n4[:2]), list(n5[:2])]
	#rot_box[p] = [list(n1[:2]), list(n2[:2])]
	return rotated, rot_box

'''
Preparing train, val and test data
'''

# vert_images = '../more_inc_height_recreate/vert_images_more_inc_height'
# img_list = os.listdir(vert_images)

# dest_train = '../more_inc_height_recreate/train'
# dest_val = '../more_inc_height_recreate/val'
# dest_test = '../more_inc_height_recreate/test'

# with open('../more_inc_height_recreate/vert_images_more_inc_height.json') as ff:
# 	data = json.load(ff)

# train_dit = {}
# val_dit = {}
# test_dit = {}

# train_list = []
# val_list = []
# test_list = []

# train_count = 0
# val_count = 0
# test_count = 0

# img_list.sort()

# for name in img_list:
# 	try:
# 		if len(name.split('_')) > 2:
# 			parent_img_name = name.split('_')[0]+'_'+name.split('_')[1]
# 		else:
# 			parent_img_name = name.split('_')[0]
		

# 		if len(train_list) < 2700:
# 			if parent_img_name in train_list:
# 				shutil.copy(os.path.join(vert_images, name), os.path.join(dest_train, name))
# 			else:
# 				train_list.append(parent_img_name)
# 				shutil.copy(os.path.join(vert_images, name), os.path.join(dest_train, name))
# 			# img = cv2.imread(os.path.join(vert_images, name))
# 			# x1,y1, x2,y2 = data[name][0][0]*img.shape[1], data[name][0][1]*img.shape[0], data[name][1][0]*img.shape[1], data[name][1][1]*img.shape[0]
# 			# if not x1 == x2:
# 			# 	m = (np.arctan((y2-y1)/(x2-x1)))/(2*3.14)
# 			# else:
# 			# 	m = (3.14/2)/(2*3.14)
# 			# dist = (np.sqrt((x1-x2)**2 + (y1-y2)**2))/img.shape[1]
# 			# gt = [[x1/img.shape[1],y1/img.shape[0]], [m,dist]]
# 			# train_dit[name] = gt
# 			train_dit[name] = data[name]

# 		elif len(val_list) < 500:
# 			if parent_img_name in val_list:
# 				shutil.copy(os.path.join(vert_images, name), os.path.join(dest_val, name))
# 			else:
# 				val_list.append(parent_img_name)
# 				shutil.copy(os.path.join(vert_images, name), os.path.join(dest_val, name))
# 			# img = cv2.imread(os.path.join(vert_images, name))
# 			# x1,y1, x2,y2 = data[name][0][0]*img.shape[1], data[name][0][1]*img.shape[0], data[name][1][0]*img.shape[1], data[name][1][1]*img.shape[0]
# 			# if not x1 == x2:
# 			# 	m = (np.arctan((y2-y1)/(x2-x1)))/(2*3.14)
# 			# else:
# 			# 	m = (3.14/2)/(2*3.14)
# 			# dist = (np.sqrt((x1-x2)**2 + (y1-y2)**2))/img.shape[1]
# 			# gt = [[x1/img.shape[1],y1/img.shape[0]], [m,dist]]
# 			# val_dit[name] = gt
# 			val_dit[name] = data[name]

# 		elif len(test_list) < 300:
# 			if parent_img_name in test_list:
# 				shutil.copy(os.path.join(vert_images, name), os.path.join(dest_test, name))
# 			else:
# 				test_list.append(parent_img_name)
# 				shutil.copy(os.path.join(vert_images, name), os.path.join(dest_test, name))
# 			# img = cv2.imread(os.path.join(vert_images, name))
# 			# x1,y1, x2,y2 = data[name][0][0]*img.shape[1], data[name][0][1]*img.shape[0], data[name][1][0]*img.shape[1], data[name][1][1]*img.shape[0]
# 			# if not x1 == x2:
# 			# 	m = (np.arctan((y2-y1)/(x2-x1)))/(2*3.14)
# 			# else:
# 			# 	m = (3.14/2)/(2*3.14)
# 			# dist = (np.sqrt((x1-x2)**2 + (y1-y2)**2))/img.shape[1]
# 			# gt = [[x1/img.shape[1],y1/img.shape[0]], [m,dist]]
# 			# test_dit[name] = gt
# 			test_dit[name] = data[name]
# 	except:
# 		print('failed')

# with open('../more_inc_height_recreate/train_dit.json', 'w') as ff:
# 	json.dump(train_dit, ff)

# with open('../more_inc_height_recreate/val_dit.json', 'w') as ff:
# 	json.dump(val_dit, ff)

# with open('../more_inc_height_recreate/test_dit.json', 'w') as ff:
# 	json.dump(test_dit, ff)



'''
Rotation augmentation with training set
'''
# train_dir = '../Data/train'
# json_path = '../Data/train_dit.json'
# dest_dir = '../Data/train_rot'
# with open(json_path) as ff:
# 	data = json.load(ff)


# rotate_levels = [-10, 0, 10]

# data_train = {}
# train_list = os.listdir(train_dir)

# for name in train_list:
# 	try:
# 		arr = cv2.imread(os.path.join(train_dir, name))
		
		
# 		for rot in rotate_levels:
# 			point = np.copy(data[name])
# 			point[0][0], point[0][1] = point[0][0]*arr.shape[1], point[0][1]*arr.shape[0]
# 			point[1][0], point[1][1] = point[1][0]*arr.shape[1], point[1][1]*arr.shape[0]
# 			rotated, rot_pnts = rotate(arr, point, rot)
# 			# x1, y1 = point[0][0]*arr.shape[1], point[0][1]*arr.shape[0]
# 			# x2, y2 = point[1][0]*arr.shape[1], point[1][1]*arr.shape[0]
# 			# cv2.circle(arr, (int(x1),int(y1)), 1, (0,255,0), 1)
# 			# cv2.circle(arr, (int(x2),int(y2)), 1, (0,255,0), 1)
# 			# print(rot_pnts)
# 			# r1, thetha1 = cart2pol(x1 - arr.shape[1]/2,y1 - arr.shape[0]/2)
# 			# r2, thetha2 = cart2pol(x2,y2)
# 			# thetha1 = np.rad2deg(thetha1)
# 			# thetha2 = np.rad2deg(thetha2)
# 			# print(thetha1, thetha2)
# 			# thetha1 += rot
# 			# x1, y1 = int(r1*np.cos(np.deg2rad(thetha1)) + arr.shape[1]/2), int(r1*np.sin(np.deg2rad(thetha1)) + arr.shape[0]/2)

# 			rot_pnts[0][0], rot_pnts[0][1] = rot_pnts[0][0]/rotated.shape[1], rot_pnts[0][1]/rotated.shape[0]
# 			rot_pnts[1][0], rot_pnts[1][1] = rot_pnts[1][0]/rotated.shape[1], rot_pnts[1][1]/rotated.shape[0]
# 			name_rot = name[:-4] + '_rot_'+str(rot)+name[-4:]
# 			cv2.imwrite(os.path.join(dest_dir, name_rot), rotated)
# 			data_train[name_rot] = rot_pnts
# 			with open('../Data/training_rot.json', 'w') as ff:
# 				json.dump(data_train, ff)
# 	except:
# 		print('failed')




'''preparing training data with contrast'''
train_dir = '../more_inc_height_recreate/Data/train'
train_dest = '../more_inc_height_recreate/Data/train_data'

json_path = '../more_inc_height_recreate/Data/train_dit.json'
with open(json_path) as ff:
	data = json.load(ff)

json_path_val = '../more_inc_height_recreate/Data/val_dit.json'
with open(json_path_val) as ff:
	data_val = json.load(ff)

train_dit = {}
train_list = os.listdir(train_dir)


clevel = [0.5, 1.0, 2.0]
sucess = 0
fail = 0

for name in train_list:
	try:
		arr = cv2.imread(os.path.join(train_dir, name))

		img_obj = Image.fromarray(arr)
		points = data[name]
		for j, level in enumerate(clevel):
			if j == 0:
				level = random.uniform(0.5, 1)
			elif j ==1:
				level = random.uniform(1,2)
			elif j == 2:
				level = random.uniform(2,3)
			else:
				level = random.uniform(3,4)
			img = ImageEnhance.Contrast(img_obj).enhance(level)
			name_level = name[:-4] + '_clevel_'+str(level)+name[-4:]
			array = np.array(img)

			name_level_flip = name_level[:-4] + '_flip'+name_level[-4:]
			array_flip = np.array(np.fliplr(array.copy()))
			shape = array.shape
			# points_flip = [[1 - points[0][0], points[0][1]], [1-points[1][0], points[1][1]]]
			# print(shape[1])
			# cv2.line(array, (int(points[0][0]*shape[1]), int(points[0][1]*shape[0])), (int(points[1][0]*shape[1]), int(points[1][1]*shape[0])), (0,255,0), 2)
			# cv2.line(array_flip, (int(points_flip[0][0]*shape[1]), int(points_flip[0][1]*shape[0])), (int(points_flip[1][0]*shape[1]), int(points_flip[1][1]*shape[0])), (255,0,0), 2)


			train_dit[name_level] = points
			# train_dit[name_level_flip] = points_flip
			# plt.figure()
			# plt.subplot(1,2,1)
			# plt.imshow(array)
			# plt.subplot(1,2,2)
			# plt.imshow(array_flip)
			# plt.show()


			cv2.imwrite(os.path.join(train_dest, name_level), array)
			# cv2.imwrite(os.path.join(train_dest, name_level_flip), array_flip)
			sucess += 1
	except:
		fail += 1
		print('failed, ', name)
		print('sucess ',sucess, ', fail ', fail )

for key, val in data_val.items():
	train_dit[key] = val

with open('../more_inc_height_recreate/Data/training_gt.json', 'w') as ff:
	json.dump(train_dit, ff)








'''
Add padding to make size equal
'''


# train_dir = '/SSD1TB/Badri/Thecal_sac/disc_images_for_thecal_sac'
# # val_dir = '/SSD1TB/Badri/Thecal_sac/Data/val'
# # test_dir = '/SSD1TB/Badri/Thecal_sac/Data/test'

# train_json = '/SSD1TB/Badri/Thecal_sac/thecal_sac_train_data.json'
# # val_json = '/SSD1TB/Badri/Thecal_sac/Data/val_dit.json'
# # test_json = '/SSD1TB/Badri/Thecal_sac/Data/test_dit.json'

# with open(train_json) as ff:
# 	data_train = json.load(ff)

# # with open(val_json) as ff:
# # 	data_val = json.load(ff)

# # with open(test_json) as ff:
# # 	data_test = json.load(ff)

# dest_path = '/SSD1TB/Badri/Thecal_sac/padded_img'
# dit = {}

# for key, val in data_train.items():
# 	img = cv2.imread(os.path.join(train_dir, key))
# 	h, w = img.shape[0], img.shape[1]

# 	if w > h:
# 		padd = int(abs(w-h)/2)
# 		zeros = np.zeros((img.shape[1], img.shape[1], 3))
# 		zeros[padd:padd+h,:,:] = img.copy()
# 		zeros = zeros.astype(np.uint8)
# 		zeros = zeros.copy()
# 		point = data_train[key]

# 		x1, y1 = int(point[0][0]*img.shape[1]), int(point[0][1]*img.shape[0])
# 		x2, y2 = int(point[1][0]*img.shape[1]), int(point[1][1]*img.shape[0])
# 		y1+=padd
# 		y2+=padd
# 		# plt.figure()
# 		# print(zeros.shape, x1,y1)
# 		# cv2.circle(zeros, (x1,y1), 1, (255,0,0), 1)
# 		# cv2.circle(zeros, (x2,y2), 1, (255,0,0), 1)
# 		# print(np.max(zeros), np.min(zeros))
# 		# plt.imshow(zeros)
# 		# plt.show()

# 	elif h > w:
# 		padd = int(abs(w-h)/2)
# 		zeros = np.zeros((img.shape[0], img.shape[0], 3))
# 		print(padd, zeros.shape, img.shape)
# 		zeros[:,padd:padd+w,:] = img.copy()
# 		zeros = zeros.astype(np.uint8)
# 		zeros = zeros.copy()
# 		point = data_train[key]

# 		x1, y1 = int(point[0][0]*img.shape[1]), int(point[0][1]*img.shape[0])
# 		x2, y2 = int(point[1][0]*img.shape[1]), int(point[1][1]*img.shape[0])
# 		x1+=padd
# 		x2+=padd
# 		# plt.figure()
# 		# print(zeros.shape, x1,y1)
# 		# cv2.circle(zeros, (x1,y1), 1, (255,0,0), 1)
# 		# cv2.circle(zeros, (x2,y2), 1, (255,0,0), 1)
# 		# print(np.max(zeros), np.min(zeros))
# 		# plt.imshow(zeros)
# 		# plt.show()
# 	pit = [[x1/img.shape[1], y1/img.shape[0]], [x2/img.shape[1], y2/img.shape[0]]]
# 	dit[key] = pit

# 	cv2.imwrite(os.path.join(dest_path, key), zeros)