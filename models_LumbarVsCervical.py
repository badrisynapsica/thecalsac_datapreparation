import os
import model_Lumbar_Cervical as models
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as f
import torch.nn.parallel
import torch.optim
import torch.optim as optim
import torch.utils.data
import torch.utils.data
import torch.utils.data.distributed
import torch.utils.data.distributed
import torchvision.datasets as datasets
import torchvision.transforms as transforms



# model_path = str('./models/')


class Data_Loader():
    def load_inference(self, root_dir):
        valdir = os.path.join(root_dir, 'eval_lumbarVscervical')

        val_dataset = datasets.ImageFolder(
            valdir,
            transforms.Compose([
                transforms.Resize((256, 256)),
                transforms.ToTensor()
            ]))

        val_loader = torch.utils.data.DataLoader(
            val_dataset, batch_size=1, shuffle=False)

        # dataiter = iter(train_loader)
        # images, labels = dataiter.next()
        # self.imshow(torchvision.utils.make_grid(images))
        return val_loader

    def imshow(self, img):
        # img = img / 2 + 0.5     # unnormalize
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))
        plt.show()


class eval_vert_seg():

    def eval(self, folder):
        #model = models.AlexNet()
        #checkpoint = torch.load('/SSD1TB/Badri/Segregate_axials_cervical_lumbar_model/ckpt/best_so_far.pth', map_location='cpu')
        #model.load_state_dict(checkpoint['state_dict'])

        model = models.AlexNet()
        base_path = os.path.abspath(os.path.dirname(__file__))
        checkpoint = torch.load(os.path.join(base_path, './ckpt/vertebra_levels_lumbarVsCervical.pth'), map_location='cpu')
        #checkpoint = torch.load('/SSD1TB/Badri/Segregate_axials_cervical_lumbar_model/ckpt/best_so_far.pth', map_location='cpu')
        model.load_state_dict(checkpoint['state_dict'])
        model.eval()
        eval_loader = Data_Loader().load_inference(folder)
        for i, (input, target) in enumerate(eval_loader):
            output = model(input)
            _, pred = output.topk(1, 1, True, True)
            pred = pred.t()
        return pred.item()
