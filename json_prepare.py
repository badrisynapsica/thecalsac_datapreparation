''' Makes a parent json from all the json available'''


import json
import os
import shutil
import matplotlib.pyplot as plt
import base_step1_essentials as essentials
import cv2
import yolo_inference
import models_LumbarVsCervical
from PIL import Image
import PIL
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
from torch.utils.data import DataLoader
import torchvision
import pydicom
import models_vertebra_segmentation
yolo_inference_disc = yolo_inference.yolo_inference(weightsPath='./ckpt/yolov3-disc-20000.weights',
													configPath='./ckpt/yolov3-disc.cfg',
													labelsPath='./ckpt/yolov3-disc.names')

yolo_inference_vertebra = yolo_inference.yolo_inference(weightsPath='./ckpt/yolov3-vertebra-89000.weights',
													configPath='./ckpt/yolov3-vertebra.cfg',
													labelsPath='./ckpt/yolov3-vertebra.names')



def between_points(line, points):
	if line[0][1] >= points[2][1] and line[0][1] <= points[3][1]:
		return True
	return False

def is_inside(rect, line):
	if not (rect[0] < line[0][0] and (rect[0]+rect[2]) > line[0][0] and rect[0] + rect[2] > line[1][0] and (rect[0]) < line[1][0]):
		return False
	if not (rect[1] < line[0][1] and (rect[1]+rect[3]) > line[0][1] and (rect[1] + rect[3]) > line[1][1] and (rect[1]) < line[1][1]):
		return False
	return True



def get_vertebra_levels(img_path):
	"""
	To get spine image type - lumbar Vs Cervical. Used to auto-label vertebra levels (C1-C2... or L1-L2....)
	:param img_path: path of the input image.
	:return: string. lumbar or cervical spine
	"""

	shutil.copyfile(img_path, './inference/eval_lumbarVscervical/pos/clipped.png')
	data_dir = "./inference/"
	output = models_LumbarVsCervical.eval_vert_seg().eval(data_dir)

	os.remove('./inference/eval_lumbarVscervical/pos/clipped.png')

	if output is 1:

		return 'lumbar'
	else:
		return 'cervical'



def add_missing_yolo_detection(out, img, spine_level, thr):
    if len(out) == 1:
        return out, []

    out.sort(key=lambda x: x[1])
    min_thr = int(0.03125*img.shape[0])
    top_left = [[curr[0], curr[1]] for curr in out]
    btm_left = [[curr[0], curr[1] + curr[3]] for curr in out]
    top_right = [[curr[0] + curr[2], curr[1]] for curr in out]
    dist_calc = essentials.vertebra_disease_eval()
    list_dist = []
    # pdb.set_trace()
    for i in range(len(top_left)-1):
        line_m, line_c = dist_calc.slope_intercept_from_points(top_left[i+1], top_right[i+1])
        perp_dist = dist_calc.dist_of_pt_from_line(line_m, line_c, btm_left[i])
        list_dist.append(perp_dist * (-1 if btm_left[i][1] > top_left[i+1][1] else 1))
    list_dist.append(list_dist[-2])

    added_idx = []
    for i in range(1, len(list_dist)-1):
        if list_dist[i] > min_thr and list_dist[i] > list_dist[i-1] and list_dist[i] > list_dist[i+1] and \
                list_dist[i] > thr*(list_dist[i - 1] + list_dist[i + 1]):
            added_idx.append(i+1)

    new_added_idx = []
    for idx, o_pos in enumerate(added_idx):
        i = o_pos + idx
        new_added_idx.append(i)
        out.insert(i, (out[i-1][0], out[i-1][1] + out[i-1][3], abs(out[i][0] + out[i][2] - out[i-1][0]),
                       abs(out[i][1] - out[i-1][1] - out[i-1][3])))

    return out, new_added_idx



# json_dir = '../Thecal_sac_tags'
# json_list = os.listdir(json_dir)

# final_dit = {}


# for json_name in json_list:
# 	with open(os.path.join(json_dir, json_name)) as ff:
# 		data = json.load(ff)
# 	data = data['_via_img_metadata']
# 	for key, val in data.items():
# 		filename = val['filename']
# 		try:
# 			regions = val['regions']
# 			points = []
# 			for re in regions:
# 				try:
# 					name_check = re['region_attributes']['name']
# 					if 'discard' in name_check.lower():
# 						continue
# 				except:
# 					print('')
# 				# print(re)
# 				all_points_x = re['shape_attributes']['all_points_x']
# 				all_points_y = re['shape_attributes']['all_points_y']

# 				point = [[all_points_x[0], all_points_y[0]], [all_points_x[1], all_points_y[1]]]
# 				points.append(point)
# 			final_dit[filename] = points
# 		except:
# 			print('Unexpected key appeared')


# with open('../Thecal_sac_tags/complete_tags.json', 'w') as ff:
# 	json.dump(final_dit, ff)




#Visualize thecal Sac data with tags

# dest_img = '../vert_images'

# dict_data = {}

# with open('../Thecal_sac_tags/complete_tags.json') as ff:
# 	data = json.load(ff)


# for key, val in data.items():
# 	try:
# 		img = cv2.imread(os.path.join('../Parent_Data/', key))
# 		img_copy = cv2.imread(os.path.join('../Parent_Data/', key))
# 		out, img_with_yolo = yolo_inference_disc.get_vert_boxes(img)
# 		if out:
# 			for i, each in enumerate(out):
# 				rect = []
# 				x = each[0]
# 				y = each[1]
# 				w = each[2]
# 				h = each[3]

# 				cv2.rectangle(img, (x, y), (x+int(w), y+int(h)), (255,0,0),2)

# 				# x += int(0.7*w)
# 				y -= int(0.4*h)
# 				h = int(1.8*h)
# 				w += int(1.5*w)
# 				rect.append(x)
# 				rect.append(y)
# 				rect.append(w)
# 				rect.append(h)
				

# 				for points in val:
# 					if is_inside(rect, points):
# 						# cv2.rectangle(img, (x,y), (x+w, y+h), (0,255,0), 2)
# 						# cv2.line(img, (int(points[0][0]), int(points[0][1])), (int(points[1][0]), int(points[1][1])), (0,0,255), 2)
						
# 						vert_img = img_copy[rect[1]:rect[1]+rect[3], rect[0]:rect[0]+rect[2]]
# 						if vert_img.shape[0] ==0 or vert_img.shape[1] ==0:
# 							continue

# 						name = key[:-4] + '_' + str(i) + key[-4:]
# 						dict_data[name] = [[(int(points[0][0]) - rect[0])/vert_img.shape[1], (int(points[0][1]) - rect[1])/vert_img.shape[0]], [(int(points[1][0]) - rect[0])/vert_img.shape[1], (int(points[1][1]) - rect[1])/vert_img.shape[0]]]
# 						cv2.imwrite(os.path.join(dest_img, name), vert_img)

# 						with open('../points_data.json', 'w') as file:
# 							json.dump(dict_data, file)

# 	except:
# 		print('failed')

# 	# for points in val:
# 	# 	cv2.line(img, (int(points[0][0]), int(points[0][1])), (int(points[1][0]), int(points[1][1])), (0,0,255), 2)
# 	# plt.figure()
# 	# print(key)
# 	# plt.imshow(img)
# 	# plt.show()





#prepare dataset using json for 6points

# dest_img = '../vert_images'

# dict_data = {}

# with open('../Thecal_sac_tags/complete_tags.json') as ff:
# 	data = json.load(ff)

# with open('../spine_level_and_6points_info.json') as jjson:
# 	seg_gt = json.load(jjson)


# for key, val in data.items():
# 	try:
# 		img = cv2.imread(os.path.join('../Parent_Data/', key))
# 		img_copy = cv2.imread(os.path.join('../Parent_Data/', key))
# 		out, img_with_yolo = yolo_inference_disc.get_vert_boxes(img)
		
# 		if out:
# 			for i, each in enumerate(out):
# 				rect = []
# 				x = each[0]
# 				y = each[1]
# 				w = each[2]
# 				h = each[3]

# 				cv2.rectangle(img, (x, y), (x+int(w), y+int(h)), (255,0,0),2)

# 				# x += int(0.7*w)
# 				y -= int(0.4*h)
# 				h = int(1.8*h)
# 				w += int(1.5*w)
# 				rect.append(x)
# 				rect.append(y)
# 				rect.append(w)
# 				rect.append(h)
				

# 				for points in val:
# 					if is_inside(rect, points):
# 						# cv2.rectangle(img, (x,y), (x+w, y+h), (0,255,0), 2)
# 						# cv2.line(img, (int(points[0][0]), int(points[0][1])), (int(points[1][0]), int(points[1][1])), (0,0,255), 2)
						
# 						vert_img = img_copy[rect[1]:rect[1]+rect[3], rect[0]:rect[0]+rect[2]]
# 						if vert_img.shape[0] ==0 or vert_img.shape[1] ==0:
# 							continue

# 						name = key[:-4] + '_' + str(i) + key[-4:]
# 						dict_data[name] = [[(int(points[0][0]) - rect[0])/vert_img.shape[1], (int(points[0][1]) - rect[1])/vert_img.shape[0]], [(int(points[1][0]) - rect[0])/vert_img.shape[1], (int(points[1][1]) - rect[1])/vert_img.shape[0]]]
# 						cv2.imwrite(os.path.join(dest_img, name), vert_img)

# 						with open('../points_data.json', 'w') as file:
# 							json.dump(dict_data, file)

# 	except:
# 		print('failed')




# Combine json of train and validation


# train_dit = '../Data/Dataset/train_gt.json'
# val_dit = '../Data/Dataset/val_dit.json'
# train_dir = '../Data/Dataset/train/pos'
# val_dir = '../Data/Dataset/val/pos'


# train_list = os.listdir(train_dir)
# val_list = os.listdir(val_dir)
# with open(train_dit) as ff:
# 	train = json.load(ff)
# with open(val_dit) as dd:
# 	val = json.load(dd)



# dit = {}
# for key, value in train.items():
# 	img = cv2.imread(os.path.join(train_dir, key))
# 	value[0][0] = value[0][0]/img.shape[1]
# 	value[0][1] = value[0][1]/img.shape[0]

# 	value[1][0] = value[1][0]/img.shape[1]
# 	value[1][1] = value[1][1]/img.shape[0]
# 	dit[key] = value
# for key, value in val.items():
# 	try:
# 		img = cv2.imread(os.path.join(val_dir, key))
# 		value[0][0] = value[0][0]/img.shape[1]
# 		value[0][1] = value[0][1]/img.shape[0]

# 		value[1][0] = value[1][0]/img.shape[1]
# 		value[1][1] = value[1][1]/img.shape[0]
# 		dit[key] = value
# 	except:
# 		print(key)
# 		if os.path.exists(os.path.join(val_dir, key)):
# 			os.remove(os.path.join(val_dir, key))

# with open('../Data/Dataset/ground_truth.json', 'w') as ff:
# 	json.dump(dit, ff)



'''
Prepare json with spine level and all vertebra's 6 points 
'''

# parent_dir = '../Parent_Data'

# with open('../Thecal_sac_tags/complete_tags.json') as ff:
# 	data = json.load(ff)


# ditt = {}
# if os.path.exists('../spine_level_and_6points_info.json'):
# 	with open('../spine_level_and_6points_info.json') as ff:
# 		ditt = json.load(ff)

# completed_processing = {}
# ii = 0
# for key, val in data.items():
# 	try:
# 		print(key, ii)
# 		ii += 1
# 		img_path = os.path.join(parent_dir, key)


# 		level = get_vertebra_levels(img_path)

# 		eval_reg = 	models_vertebra_segmentation.eval_vert_seg(level)

# 		img = cv2.imread(os.path.join(parent_dir, key))
# 		img_copy = cv2.imread(os.path.join(parent_dir, key))
# 		out, img_with_yolo = yolo_inference_vertebra.get_vert_boxes(img)

# 		out, added_idx = add_missing_yolo_detection(out, img, level, 2)

# 		if len(added_idx) > 0:
# 			continue


# 		out_new = []
# 		disc_points = []
# 		disc_points.append(level)
# 		disc_points.append([])
# 		for i, each in enumerate(out):
# 			h_change = 15
# 			w_change = each[2] * 0.8

# 			x = int(each[0] - w_change / 2)
# 			y = int(each[1] - h_change / 2)
# 			w = int(each[2] + w_change)
# 			h = int(each[3] + h_change)
# 			out_new = (x, y, w, h)

# 			vert_img = img[max(0, y):max(0, y) + h, max(x, 0):max(0, x) + w]
# 			# print(vert_img.shape)
# 			cv2.imwrite('./inference/eval_lumbarVscervical/pos/vert.jpg', vert_img)

# 			pred = eval_reg.inference()

# 			x1, y1 = x+int(pred[0]*vert_img.shape[1]), max(0,y+int(pred[1]*vert_img.shape[0]))
# 			x2, y2 = x+int(pred[2]*vert_img.shape[1]), max(0,y+int(pred[3]*vert_img.shape[0]))
# 			x3, y3 = x+int(pred[4]*vert_img.shape[1]), max(0,y+int(pred[5]*vert_img.shape[0]))
# 			x4, y4 = x+int(pred[6]*vert_img.shape[1]), max(0,y+int(pred[7]*vert_img.shape[0]))
# 			x5, y5 = x+int(pred[8]*vert_img.shape[1]), max(0,y+int(pred[9]*vert_img.shape[0]))
# 			x6, y6 = x+int(pred[10]*vert_img.shape[1]),max(0,y+ int(pred[11]*vert_img.shape[0]))

# 			cv2.circle(img_copy, (x1,y1), 2, (255,0,0), 2)
# 			cv2.circle(img_copy, (x2,y2), 2, (255,0,0), 2)
# 			cv2.circle(img_copy, (x3,y3), 2, (255,0,0), 2)
# 			cv2.circle(img_copy, (x4,y4), 2, (255,0,0), 2)
# 			cv2.circle(img_copy, (x5,y5), 2, (255,0,0), 2)
# 			cv2.circle(img_copy, (x6,y6), 2, (255,0,0), 2)


# 			disc_points[1].append([[x1,y1], [x2,y2], [x3,y3], [x4,y4], [x5,y5], [x6,y6]])

# 		if os.path.exists('../spine_level_and_6points_info.json'):
# 			with open('../spine_level_and_6points_info.json') as ff:
# 				ditt = json.load(ff)
				
# 		ditt[key] = disc_points

# 		with open('../spine_level_and_6points_info.json', 'w') as f:
# 			json.dump(ditt, f)
# 	except:
# 		print('failed, ', key)
# print(len(ditt))



'''
Preparing vert images using 6points data from json
'''

ditt = {}


with open('../Thecal_sac_tags/complete_tags.json') as ff:
	data = json.load(ff)

with open('../spine_level_and_6points_info.json') as ff:
	vert_seg = json.load(ff)

dest_img = '../vert_images_more_inc_height'
parent_dir = '../Parent_Data'

for key, val in vert_seg.items():
	img_path = os.path.join(parent_dir, key)
	img = cv2.imread(img_path)
	points_thecal_sac = data[key]
	val = val[1]
	for i, v in enumerate(val):
		point = []
		if i == 0 or i == len(val)-1:
			continue
		if i == 1:
			box_up = [v[0], v[1], v[2]]
			up = [v[3], v[4], v[5]]
			continue
		else:
			point = [up[2], up[1], up[0], v[2], v[1], v[0]]
			up = [v[3], v[4], v[5]]


		#Similar conditions to mri vertebrae segmentation
		# x = min(box_up[0][0], up[2][0])
		# y = max(0,min(box_up[0][1], box_up[2][1]))
		# w = int(abs(min(box_up[0][0], up[2][0]) - max(box_up[2][0], up[0][0])))
		# h = abs(min(box_up[0][1], box_up[2][1]) - max(up[0][1], up[2][1]))
		# x -= int(0.4*w)
		# x = int(x)
		# w = int(2*abs(min(box_up[0][0], up[2][0]) - max(box_up[2][0], up[0][0])))


		#use this block for inc height disc image.
		# x = min(box_up[0][0], up[2][0])
		# y = max(0,min(box_up[0][1], box_up[2][1]))
		# w = int(abs(min(box_up[0][0], up[2][0]) - max(box_up[2][0], up[0][0])))
		# h = abs(min(box_up[0][1], box_up[2][1]) - max(up[0][1], up[2][1]))
		# x -= w
		# x = int(x)
		# w = int(3*abs(min(box_up[0][0], up[2][0]) - max(box_up[2][0], up[0][0])))



		# use this block for even more inc height
		# x = min(box_up[0][0], up[2][0])
		# y = max(0,min(box_up[0][1], box_up[2][1]))
		# w = int(abs(min(box_up[0][0], up[2][0]) - max(box_up[2][0], up[0][0])))
		# x -= w
		# x = int(x)
		# w = int(3*abs(min(box_up[0][0], up[2][0]) - max(box_up[2][0], up[0][0])))
		# h = abs(min(box_up[0][1], box_up[2][1]) - max(up[0][1], up[2][1]))
		# y -= int(0.3*h)
		# y = max(0,y)
		# h += int(0.6*h)

		#this is for normal height disc img
		x = min(point[0][0], point[5][0])
		y = max(0,min(point[0][1], point[2][1]))
		w = int(abs(min(point[0][0], point[5][0]) - max(point[2][0], point[3][0])))
		x -= w
		x = max(0,int(x))
		w = int(3*abs(min(point[0][0], point[5][0]) - max(point[2][0], point[3][0])))
		h = abs(min(point[0][1], point[2][1]) - max(point[3][1], point[5][1]))
		y -= int(h)
		y = max(0,y)
		h += int(2*h)

		rect = [x, y, w, h]

		
		for pp in points_thecal_sac:
			if  pp[0][1]>point[2][1] and pp[0][1]<point[3][1]:
				gt_thecal_sac = pp
				rect[2] += 3*abs(pp[0][0]-pp[1][0])
				disc_img = img[rect[1]:rect[1]+rect[3], rect[0]:rect[0]+rect[2]].copy()
				# cv2.line(disc_img, (gt_thecal_sac[0][0]-rect[0], gt_thecal_sac[0][1]-rect[1]), (gt_thecal_sac[1][0]-rect[0], gt_thecal_sac[1][1]-rect[1]), (255,0,0),1)
				disc_name = key[:-4]+'_'+str(i)+key[-4:]

				vert_borders = [box_up[0], box_up[2], up[0], up[2]]

				# cv2.circle(disc_img, (point[0][0]-rect[0], point[0][1]-rect[1]), 1, (0,255,0),1)
				# cv2.circle(disc_img, (point[2][0]-rect[0], point[2][1]-rect[1]), 1, (0,255,0),1)
				# cv2.circle(disc_img, (point[3][0]-rect[0], point[3][1]-rect[1]), 1, (0,255,0),1)
				# cv2.circle(disc_img, (point[5][0]-rect[0], point[5][1]-rect[1]), 1, (0,255,0),1)		
				# cv2.circle(disc_img, (gt_thecal_sac[0][0]-rect[0], gt_thecal_sac[0][1]-rect[1]), 1, (0,255,0),1)
				# cv2.circle(disc_img, (gt_thecal_sac[1][0]-rect[0], gt_thecal_sac[1][1]-rect[1]), 1, (0,255,0),1)
				# plt.figure()
				# plt.imshow(disc_img)
				# plt.show()

				# Predicting 6 points
				# corner_pnts = []	
				# corner_pnts.append([(point[0][0]-rect[0])/disc_img.shape[1], (point[0][1]-rect[1])/disc_img.shape[0]])
				# corner_pnts.append([(point[2][0]-rect[0])/disc_img.shape[1], (point[2][1]-rect[1])/disc_img.shape[0]])
				# corner_pnts.append([(point[3][0]-rect[0])/disc_img.shape[1], (point[3][1]-rect[1])/disc_img.shape[0]])
				# corner_pnts.append([(point[5][0]-rect[0])/disc_img.shape[1], (point[5][1]-rect[1])/disc_img.shape[0]])
				# corner_pnts.append([(gt_thecal_sac[0][0]-rect[0])/disc_img.shape[1], (gt_thecal_sac[0][1]-rect[1])/disc_img.shape[0]])
				# corner_pnts.append([(gt_thecal_sac[1][0]-rect[0])/disc_img.shape[1], (gt_thecal_sac[1][1]-rect[1])/disc_img.shape[0]])

				# gt = corner_pnts

				gt = [[(gt_thecal_sac[0][0]-rect[0])/disc_img.shape[1], (gt_thecal_sac[0][1]-rect[1])/disc_img.shape[0]], [(gt_thecal_sac[1][0]-rect[0])/disc_img.shape[1], (gt_thecal_sac[1][1]-rect[1])/disc_img.shape[0]]]
				cv2.imwrite(os.path.join(dest_img, disc_name), disc_img)
				if os.path.exists('../vert_images_more_inc_height.json'):
					with open('../vert_images_more_inc_height.json') as read:
						ditt = json.load(read)
				ditt[disc_name] = gt
				# else:
				with open('../vert_images_more_inc_height.json', 'w') as read:
					json.dump(ditt, read)

				break

		box_up = [v[0], v[1], v[2]]

		# cv2.rectangle(img, (rect[0], rect[1]), (rect[0]+rect[2], rect[1]+rect[3]), (255,0,0), 1)
		# cv2.line(img, (gt_thecal_sac[0][0], gt_thecal_sac[0][1]), (gt_thecal_sac[1][0], gt_thecal_sac[1][1]), (0,255,0), 1)



	# plt.figure()
	# plt.imshow(img)
	# plt.show()





'''
Preparing dataset with 6 points data and classification model using parent data .
'''
# dest_img = '../vert_images'
# parent_dir = '../Parent_Data'

# dict_data = {}

# with open('../Thecal_sac_tags/complete_tags.json') as ff:
# 	data = json.load(ff)


# ditt = {}
# completed_processing = {}

# for key, val in data.items():
# 	# try:
# 	img_path = os.path.join(parent_dir, key)


# 	level = get_vertebra_levels(img_path)

# 	eval_reg = 	models_vertebra_segmentation.eval_vert_seg(level)

# 	img = cv2.imread(os.path.join(parent_dir, key))
# 	img_copy = cv2.imread(os.path.join(parent_dir, key))
# 	out, img_with_yolo = yolo_inference_vertebra.get_vert_boxes(img)

# 	out, added_idx = add_missing_yolo_detection(out, img, level, 2)

# 	if len(added_idx) > 0:
# 		continue


# 	out_new = []
# 	disc_points = []
# 	for i, each in enumerate(out):
# 		h_change = 15
# 		w_change = each[2] * 0.8

# 		x = int(each[0] - w_change / 2)
# 		y = int(each[1] - h_change / 2)
# 		w = int(each[2] + w_change)
# 		h = int(each[3] + h_change)
# 		out_new = (x, y, w, h)

# 		vert_img = img[max(0, y):max(0, y) + h, max(x, 0):max(0, x) + w]
# 		# print(vert_img.shape)
# 		cv2.imwrite('./inference/eval_lumbarVscervical/pos/vert.jpg', vert_img)

# 		pred = eval_reg.inference()

# 		x1, y1 = x+int(pred[0]*vert_img.shape[1]), y+int(pred[1]*vert_img.shape[0])
# 		x2, y2 = x+int(pred[2]*vert_img.shape[1]), y+int(pred[3]*vert_img.shape[0])
# 		x3, y3 = x+int(pred[4]*vert_img.shape[1]), y+int(pred[5]*vert_img.shape[0])
# 		x4, y4 = x+int(pred[6]*vert_img.shape[1]), y+int(pred[7]*vert_img.shape[0])
# 		x5, y5 = x+int(pred[8]*vert_img.shape[1]), y+int(pred[9]*vert_img.shape[0])
# 		x6, y6 = x+int(pred[10]*vert_img.shape[1]),y+ int(pred[11]*vert_img.shape[0])

# 		cv2.circle(img_copy, (x1,y1), 2, (255,0,0), 2)
# 		cv2.circle(img_copy, (x2,y2), 2, (255,0,0), 2)
# 		cv2.circle(img_copy, (x3,y3), 2, (255,0,0), 2)
# 		cv2.circle(img_copy, (x4,y4), 2, (255,0,0), 2)
# 		cv2.circle(img_copy, (x5,y5), 2, (255,0,0), 2)
# 		cv2.circle(img_copy, (x6,y6), 2, (255,0,0), 2)



# 		plt.figure()
# 		plt.imshow(img_copy)
# 		plt.show()

		

# 		if i == 0:
# 			low_points = [(out_new[0] + x4, out_new[1] + y4), (out_new[0] + x5, out_new[1] + y5), (out_new[0] + x6, out_new[1] + y6)]
# 			# prev_box = [out_new]
# 			continue


# 		disc_points.append([[low_points[2][0], low_points[2][1]], [low_points[1][0], low_points[1][1]], [low_points[0][0], low_points[0][1]], [x + x3, y+y3], [x+x2, y+y2], [x+x1,y+y1]])

		
# 		low_points = [(out_new[0] + x4, out_new[1] + y4), (out_new[0] + x5, out_new[1] + y5), (out_new[0] + x6, out_new[1] + y6)]
# 		prev_box = [out_new]

# 		cv2.circle(img, (disc_points[-1][0][0], disc_points[-1][0][1]), 1, (0,0,255), 1)
# 		cv2.circle(img, (disc_points[-1][1][0], disc_points[-1][1][1]), 1, (0,0,255), 1)
# 		cv2.circle(img, (disc_points[-1][2][0], disc_points[-1][2][1]), 1, (0,0,255), 1)
# 		cv2.circle(img, (disc_points[-1][3][0], disc_points[-1][3][1]), 1, (0,0,255), 1)
# 		cv2.circle(img, (disc_points[-1][4][0], disc_points[-1][4][1]), 1, (0,0,255), 1)
# 		cv2.circle(img, (disc_points[-1][5][0], disc_points[-1][5][1]), 1, (0,0,255), 1)

# 	low = []
# 	for i, disc in enumerate(disc_points):
# 		for points in val:
# 			# cv2.line(img_copy, (points[0][0], points[0][1]), (points[1][0], points[1][1]), (0,255,0), 1)
# 			if between_points(points, disc):
# 				# print('inside')
# 				if i==0:
# 					x = min(disc[0][0],disc[1][0],disc[2][0])
# 					y = min(disc[0][1],disc[1][1],disc[2][1])
# 					w = max(disc[3][0], disc[4][0], disc[5][0]) - min(disc[0][0],disc[1][0],disc[2][0])
# 					h = min(disc[3][1], disc[4][1], disc[5][1]) - min(disc[0][1],disc[1][1],disc[2][1])
# 					x_up = x - int(0.2*w)
# 					y_up = y - int(0.6*h)
# 					w += int(1.4*w)
# 					h += int(1.4*h)
# 					x_low = x_up+w
# 					y_low = max(0,y_up) + h
# 					low = [x_up, y_up, x_low, y_low]
# 				elif i == len(disc_points)-1:
# 					x = min(disc[0][0],disc[1][0],disc[2][0])
# 					y = min(disc[0][1],disc[1][1],disc[2][1])
# 					w = max(disc[3][0], disc[4][0], disc[5][0]) - min(disc[0][0],disc[1][0],disc[2][0])
# 					h = max(disc[3][1], disc[4][1], disc[5][1]) - min(disc[0][1],disc[1][1],disc[2][1])
# 					x_up = min(x, low[0])
# 					y_up = low[3] - int(0.2*h)
# 					w += int(1.4*w)
# 					h += int(0.8*h)
# 					x_low = x_up+w
# 					y_low = max(0,y_up) + h
# 				else:
# 					x = min(disc[0][0],disc[1][0],disc[2][0])
# 					y = min(disc[0][1],disc[1][1],disc[2][1])
# 					w = max(disc[3][0], disc[4][0], disc[5][0]) - min(disc[0][0],disc[1][0],disc[2][0])
# 					h = min(disc[3][1], disc[4][1], disc[5][1]) - min(disc[0][1],disc[1][1],disc[2][1])
# 					if len(low) > 0:
# 						x_up = min(x,low[0])
# 						y_up = low[3] - int(0.4*h)
# 					else:
# 						x_up = x
# 						y_up = y - int(0.4*h)
# 					w += int(1.4*w)
# 					h += int(1.6*h)
# 					x_low = x+w
# 					y_low = max(0,y) + h
# 					low = [x_up, y_up, x_low, y_low]
# 				cv2.rectangle(img_copy, (x_up, y_up), (x_low, y_low), (255,0,0),1)
# 	plt.figure()
# 	plt.imshow(img_copy)
# 	plt.show()


				# name = key[:-4] + '_'+str(i) + key[-4:]

				# disc_img = img_copy[max(0,y_up):min(img_copy.shape[0],y_low),max(0,x_up):x_low]

				# dest_path = os.path.join('../disc_images_for_thecal_sac/', name)
				# cv2.imwrite(dest_path, disc_img)
				# point_disc = [[(points[0][0] - x_up)/disc_img.shape[1], (points[0][1] - y_up)/disc_img.shape[0]], [(points[1][0] - x_up)/disc_img.shape[1], (points[1][1] - y_up)/disc_img.shape[0]]]

				# ditt[name] = point_disc
				# with open('../thecal_sac_train_data.json', 'w') as ff:
				# 	json.dump(ditt, ff)
				# completed_processing[key] = ['True']
				# with open('../track_completed_data.json', 'w') as dd:
				# 	json.dump(completed_processing, dd)

				# cv2.circle(disc_img, (int(point_disc[0][0]*disc_img.shape[1]), int(point_disc[0][1]*disc_img.shape[0])), 1, (255,0,0),1)
				# cv2.circle(disc_img, (int(point_disc[1][0]*disc_img.shape[1]), int(point_disc[1][1]*disc_img.shape[0])), 1, (255,0,0), 1)
				# plt.figure()
				# print(disc_img.shape, x,y,w,h)
				# plt.imshow(disc_img)
				# plt.show()

				# cv2.line(img, (points[0][0], points[0][1]), (points[1][0], points[1][1]), (0,255,0), 1)
				# cv2.rectangle(img, (x,y),(x+w,y+h), (255,0,0), 1)
	# except:
	# 	print('failed on one data')
	# plt.figure()
	# plt.subplot(1,2,1)
	# plt.imshow(img)
	# plt.subplot(1,2,2)
	# plt.imshow(img_copy)
	# plt.show()

