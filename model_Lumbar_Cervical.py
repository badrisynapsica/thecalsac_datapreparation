import argparse
import os
import random
import shutil
import time
import warnings
import sys
import math
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.optim
import torch.multiprocessing as mp
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
import torchvision
import matplotlib.pyplot as plt
import numpy as np
import torch.optim as optim
import torch.nn.functional as f




class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 96, 7)
        self.pool = nn.MaxPool2d(3, 3)
        self.bnorm1 = nn.BatchNorm2d(num_features=16)
        self.conv4 = nn.Conv2d(96, 96, 7)
        self.bnorm2 = nn.BatchNorm2d(num_features=32)

        self.conv5 = nn.Conv2d(96, 96, 5)
        self.conv6 = nn.Conv2d(96, 96, 5)
        self.conv7 = nn.Conv2d(96, 128, 5)
        self.conv8 = nn.Conv2d(128, 128, 5)
        self.conv9 = nn.Conv2d(128, 256, 5)
        # self.conv10 = nn.Conv2d(256, 256, 5)
        # self.conv11 = nn.Conv2d(256, 256, 5)
        # self.conv12 = nn.Conv2d(256, 256, 5)
        # self.fc1 = nn.Linear(73984 , 73984)
        self.fc2 = nn.Linear(73984 , 256)
        self.fc3 = nn.Linear(256, 128)
        self.fc4 = nn.Linear(128, 2)

    def forward(self, x):
        x = f.relu(self.conv1(x))
        x = f.local_response_norm(x,5,0.001,beta=0.75)
        x = self.pool(x)

        # x = self.bnorm1(x)
        # x = f.relu(self.conv2(x))
        # x = f.relu(self.conv3(x))
        # x = f.relu(self.conv3(x))
        x = f.relu(self.conv4(x))
        # x = f.local_response_norm(x, 5, 0.001)
        # x = self.pool(x)
        x = f.relu(self.conv5(x))

        # x = self.bnorm2(x)
        x = f.relu(self.conv6(x))
        # x = f.local_response_norm(x,5,0.001,beta=0.75)
        # x = self.pool(x)

        # x = self.bnorm1(x)
        x = f.relu(self.conv6(x))
        x = f.relu(self.conv7(x))
        x = f.relu(self.conv8(x))
        x = f.relu(self.conv9(x))
        # x = f.relu(self.conv10(x))
        # x = f.relu(self.conv11(x))
        # x = f.relu(self.conv12(x))
        x = f.local_response_norm(x,5,0.001,beta=0.75)
        x = self.pool(x)
        # x = self.bnorm1(x)
        x = x.view(-1, self.num_flat_features(x))
        # x = f.relu(self.fc1(x))
        # x = f.dropout2d(x,p=0.5)
        x = f.relu(self.fc2(x))
        # x = f.dropout2d(x,p=0.5)
        x = f.relu(self.fc3(x))
        x = self.fc4(x)
        # x = f.dropout2d(x,p=0.5)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        # print(num_features)
        return num_features

class AlexNet(nn.Module):

    def __init__(self, num_classes=2):
        super(AlexNet, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=11, stride=4, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(64, 192, kernel_size=5, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(192, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
        )
        self.avgpool = nn.AdaptiveAvgPool2d((6, 6))
        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(256 * 6 * 6, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096, num_classes),
        )

    def forward(self, x):
        x = self.features(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), 256 * 6 * 6)
        x = self.classifier(x)
        return x
