__description__ = "Script for different file related operations like dicom file reading, save dictionary as json, load jsons...."

import json
import os
import pickle
from operator import itemgetter

import cv2
import numpy as np
import pandas as pd
import pydicom

# import base_step1_functions_file as functions_file
# import base_step1_mid_saggital as mid_saggital
# import base_step1_sag_functions as sag_functions

parent_dir = os.getcwd()

import logging

logger = logging.getLogger('tdm')


class file_operations:

	def read_csv(self, filename):
		"""

		:param filename: csv filename
		:return: list of csv columns
		"""
		data = pd.read_csv(filename, header=None)
		return list(data[0])

	def dicom2jpg_clipped(self, dicom_file):
		"""

		:param dicom_file: dicom file-name
		:return: numpy array after remapping dicom values to 0-255
		"""
		pdm_img = pydicom.dcmread(dicom_file)
		try:
			if pdm_img.pixel_array:
				pass
		except NotImplementedError:
			pdm_img.decompress()
		ax_file_jpg = pdm_img.pixel_array
		# img1 = np.clip(ax_file_jpg, 100, 1200)
		rgb_new_img = functions_file.remap_255(ax_file_jpg)
		return rgb_new_img

	def dicom_loader(self, filepath):
		"""

		:param filepath: folder containing all the dicom files
		:return: ordered list of sagittal and axial files
		"""
		sag_files, ax_files, sag_seq, ax_seq, sag_files_sop, ax_files_sop, dict_series_UID = mid_saggital.t2_sag_axial(filepath, 0, 0, False)
		if sag_files==None and ax_files==None and sag_seq==None and ax_seq==None and sag_files_sop==None and ax_files_sop==None and dict_series_UID==None:
			sag_files, ax_files, sag_seq, ax_seq, sag_files_sop, ax_files_sop, dict_series_UID = mid_saggital.t2_sag_axial(filepath, 0, 0, True)
		if sag_files is not None:
			sag_files = mid_saggital.sort_sag(sag_files)
			if ax_files is not None:
				ax_files = mid_saggital.sort_ax(ax_files)
		# print('no of sag files ', len(sag_files))
		# print('no of ax files ', len(sag_files))
		return sag_files, ax_files, sag_seq, ax_seq, sag_files_sop, ax_files_sop, dict_series_UID

	def dicom2jpg(self, dicom_file, jpg_folder_path):
		"""
		Return multiple numpy arrays for the given dicom file
		:param dicom_file: dicom file path
		:param jpg_folder_path: path to save the jpeg image, useless for now
		:return: numpy array after remapping to 0-255, ds.numpy_array. numpy array after remapping to 0-255
		"""
		print('dicom file',dicom_file)
		ds = pydicom.dcmread(dicom_file)
		jpg_path = str(parent_dir + '/' + jpg_folder_path)
		if not (os.path.exists(jpg_path)):
			print('creating folder for jpeg file storage!!!!')
			os.mkdir(jpg_path)
		PNG = False
		try:
			if len(ds.pixel_array.shape) > 0:
				pass
		except NotImplementedError:
			ds.decompress()

		pixel_array_numpy = ds.pixel_array
		if not PNG:
			dicom_file_jpg = dicom_file.replace('.dcm', '.jpg')
		else:
			dicom_file_jpg = dicom_file.replace('.dcm', '.png')
		dicom_file_jpg = os.path.basename(dicom_file_jpg)
		if 'jpg' in dicom_file_jpg:
			temp = 0
		else:
			dicom_file_jpg = str(dicom_file_jpg + '.jpg')

		rgb_new_img = functions_file.remap_255(pixel_array_numpy)
		# rgb_new_img = cv2.equalizeHist(rgb_new_img)
		# rgb_new_img = cv2.adaptiveThreshold(rgb_new_img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,43,6)
		# cv2.imshow('rgb ',temp)
		# cv2.waitKey(0)

		cv2.imwrite(os.path.join(jpg_path, dicom_file_jpg), rgb_new_img)
		# cv2.imshow('np ',pixel_array_numpy)
		# cv2.waitKey(0)

		temp = cv2.imread(os.path.join(jpg_path, dicom_file_jpg))
		os.remove(os.path.join(jpg_path, dicom_file_jpg))
		# cv2.imshow('temp ',temp)
		# cv2.waitKey(0)
		# img_temp = np.clip(pixel_array_numpy, 100, 1200)
		# cv2.imshow('r',rgb_new_img)
		# cv2.waitKey(0)

		return temp, pixel_array_numpy,rgb_new_img

	def dict2json(self, sample, filename):
		"""
		Save Dictionary as Json
		:param sample:dictionary to be exported
		:param filename: filename of output json (without extension)
		:return: None
		"""
		with open(str(filename + '.json'), 'w') as fp:
			json.dump(sample, fp)

	def json2dict(self, filename):
		"""
		Load Json as Dictionary
		:param filename: Json filename to be loaded
		:return: dictionary
		"""
		with open(filename) as f:
			data = json.load(f)
		return data

	def dict2pickle(self, sample, filename):
		"""
		Save Dictionary as Pickle
		:param sample:dictionary to be imported
		:param filename: filename of output Pickle (without extension)
		:return: None
		"""
		with open(str(filename + '.pickle'), 'wb') as handle:
			pickle.dump(sample, handle, protocol=pickle.HIGHEST_PROTOCOL)

	def pickle2dict(self, filename):
		"""
		Load Pickle as Dictionary
		:param filename: Pickle filename to be loaded
		:return: dictionary
		"""
		with open(str(filename), 'rb') as handle:
			sample = pickle.load(handle)
		return sample


class spine_dicomloader:

	def input_data(self, folder):
		"""
		:param folder:
		:return sag_files: list of sagittal file names
		:return sag_file_jpg: numpy array for mid-sagittal dcm
		:return ax_files: list of axial file names
		:return lines_data_sorted: list of line co-ordinates; projection of axial files on sagittal
		:return line_filename: dictionary of lines; Key is line co-ordinates, Value is axial file-name
		:return crop_size_array: (xmin,ymin,xmax,ymax) - first and last line coordinates
		:return sag_file_jpg_org: mid-sag file with clipping
		:return ds_numpy: ds.numpy_array for mid-sagittal
		:return clipped_org: ds.numpy_array + remap(255) for mid-sagittal
		"""
		sag_files, ax_files, sag_seq, ax_seq, sag_files_sop, ax_files_sop, dict_series_UID = file_operations().dicom_loader(folder)
		if sag_files is None:
			return None, None, None, None, None, [], None, None, None, None, None, None, None, None

		sag_file = sag_files[int(len(sag_files) / 2)]

		jpg_folder_name = ''
		sag_file_jpg, ds_numpy,clipped_org = file_operations().dicom2jpg(sag_file, jpg_folder_name)
		if ax_files is not None:
			_, ymin, ymax, xmin, xmax = sag_functions.clip_on_axial(sag_file, ax_files[0], ax_files[len(ax_files) - 1])
		
		else:
			ymin, ymax, xmin, xmax = 0, 0, 0, 0
		sag_file_jpg_org = np.copy(sag_file_jpg)
		ymin = max(0, ymin)
		xmin = max(0, xmin)

		sag_file_jpg = sag_file_jpg[ymin:ymax, xmin:xmax]

		line_filename = {}
		lines_data_sorted = []

		if ax_files is not None:
			for path in ax_files:
				tlhc, trhc, blhc, brhc = sag_functions.draw_ax_line(sag_file, path)
				tlhc[1] -= ymin
				tlhc[0] -= xmin
				trhc[1] -= ymin
				trhc[0] -= xmin
				blhc[1] -= ymin
				blhc[0] -= xmin
				brhc[1] -= ymin
				brhc[0] -= xmin
				mid_tlhc = [int((trhc[0] + tlhc[0]) / 2), int((trhc[1] + tlhc[1]) / 2)]
				mid_blhc = [int((brhc[0] + blhc[0]) / 2), int((brhc[1] + blhc[1]) / 2)]
				line_filename[(mid_tlhc[0], mid_tlhc[1], mid_blhc[0], mid_blhc[1])] = path

				mid_pt_left = int((mid_tlhc[1] + mid_blhc[1]) / 2)
				lines_data_sorted.append([mid_tlhc, mid_blhc, mid_pt_left])
				line_filename[(mid_tlhc[0], mid_tlhc[1], mid_blhc[0], mid_blhc[1])] = path

				# print("tlhc: ", tlhc, " trhc: ", trhc, " blhc: ", blhc, " brhc: ", brhc)
				# print(" mid_tlhc: ", mid_tlhc, " mid_blhc: ", mid_blhc, " mid_pt_left: ", mid_pt_left)

			lines_data_sorted = sorted(lines_data_sorted, key=itemgetter(2))
		# print("spine_dicomloader:input_data.........", lines_data_sorted)
		return sag_files, sag_file_jpg, ax_files, lines_data_sorted, line_filename, [xmin, ymin, xmax,ymax], \
				sag_file_jpg_org, ds_numpy, clipped_org, sag_seq, ax_seq, sag_files_sop, ax_files_sop, dict_series_UID


class vertebra_disease_eval():

	def between_points(self, point, left, right):
		return (point[0] >= left[0]) and (point[0] <= right[0])

	def slope_intercept_from_points(self, point1, point2):
		x = 0
		y = 1
		if point1[x] == point2[x]:
			m = None
			c = point1[x]
		else:
			m = (point2[y] - point1[y]) / (point2[x] - point1[x])
			c = point2[y] - m * point2[x]
		return m, c

	def slope_intercept_of_perpendicular(self, m_org, perp_pt):
		x = 0
		y = 1
		m = 0.0 if m_org is None else (-1 / m_org)
		c = perp_pt[y] - m * perp_pt[x]
		return m, c

	def intersection_of_lines(self, line1_m, line1_c, line2_m, line2_c):
		if line1_m is None:
			x = line1_c
			y = line2_m * x + line2_c
		elif line2_m is None:
			x = line2_c
			y = line1_m * x + line1_c
		else:
			x = (line1_c - line2_c) / (line2_m - line1_m)
			y = line2_m * x + line2_c
		return x, y

	def dist_between_pts(self, point1, point2):
		x = 0
		y = 1
		return np.sqrt((point1[x] - point2[x]) ** 2 + (point1[y] - point2[y]) ** 2)

	def intersection_with_perpendicular(self, line_m, line_c, perp_pt):
		if line_m is None:
			intersect_x = line_c
			intersect_y = perp_pt[1]
		elif line_m == 0.0 or line_m == -0.0:
			intersect_x = perp_pt[0]
			intersect_y = line_c
		else:
			perp_m, perp_c = self.slope_intercept_of_perpendicular(line_m, perp_pt)
			intersect_x, intersect_y = self.intersection_of_lines(perp_m, perp_c, line_m, line_c)
		return intersect_x, intersect_y

	def draw_vertebrae_area_ratio_lines(self, vert1_pt3, intersection_pt, vert2_pt0, vert2_pt2, input_filename):
		img = cv2.imread('./output/' + input_filename + '_vert_zmap.jpg')
		if not os.path.exists('./output/' + input_filename + '_vert_zmap_pts.jpg'):
			shutil.copyfile('./output/' + input_filename + '_vert_zmap.jpg',
							'./output/' + input_filename + '_vert_zmap_pts.jpg')
		cv2.line(img, intersection_pt, vert2_pt2, (255, 255, 255), 1)
		cv2.line(img, vert2_pt0, vert2_pt2, (255, 255, 255), 1)
		cv2.line(img, vert1_pt3, intersection_pt, (255, 255, 255), 1)

		cv2.imwrite('./output/' + input_filename + '_vert_zmap.jpg', img)

	def cob_angle(self, points):

		angle_up = []
		angle_down = []

		for point in points:
			angle = np.rad2deg(math.atan((point[2][1] - point[0][1]) / (point[2][0] - point[0][0])))
			angle_up.append(angle)
			angle = np.rad2deg(math.atan((point[3][1] - point[5][1]) / (point[3][0] - point[5][0])))
			angle_down.append(angle)

		if np.argmax(np.array(angle_up)) < np.argmin(np.array(angle_down)):
			print('Angle made by axial line is ', max(angle_up) - min(angle_down),
				  'degrees, the corresponding lines are ', np.argmax(np.array(angle_up)), 'and ',
				  np.argmin(np.array(angle_down)))
		else:
			return 0, (0, 0)
		''' 
		Ensure that min slope occurs before max slope, Also ensure that intersection occurs at right side
		'''
		return max(angle_up) - min(angle_down), (np.argmin(np.array(angle_down)), np.argmax(np.array(angle_up)))

	def midpoint(self, point1, point2):
		return (point1[0] + point2[0]) / 2, (point1[1] + point2[1]) / 2

	def dist_of_pt_from_line(self, line_m, line_c, point):
		intersect_x, intersect_y = self.intersection_with_perpendicular(line_m, line_c, point)
		return self.dist_between_pts((intersect_x, intersect_y), point)

	def disc_height(self, vert1, vert2):
		# superior edge of vertebra below
		superior_m, superior_c = self.slope_intercept_from_points(vert2[2], vert2[0])
		intersect_x, intersect_y = self.intersection_with_perpendicular(superior_m, superior_c, vert1[4])
		return self.dist_between_pts((intersect_x, intersect_y), vert1[4]), self.dist_between_pts(vert1[4], vert2[1])

	def disc_height_dcra_mod(self, vert1, vert2):
		mid_plane_pt1 = self.midpoint(vert1[5], vert2[0])
		mid_plane_pt2 = self.midpoint(vert1[3], vert2[2])

		# midline between inferior edge of vert1 and superior edge of vert2
		mid_plane_m, mid_plane_c = self.slope_intercept_from_points(mid_plane_pt1, mid_plane_pt2)
		ant_height = self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert1[5]) + \
			self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert2[0])
		mid_height = self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert1[4]) + \
			self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert2[1])
		post_height = self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert1[3]) + \
			self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert2[2])
		return ant_height, mid_height, post_height

	def vert_height(self, vert):
		ant_ht = self.dist_between_pts(vert[0], vert[5])
		post_ht = self.dist_between_pts(vert[2], vert[3])
		mid_ht = self.dist_between_pts(vert[1], vert[4])

		mid_plane_pt1 = self.midpoint(vert[0], vert[5])
		mid_plane_pt2 = self.midpoint(vert[2], vert[3])
		mid_plane_m, mid_plane_c = self.slope_intercept_from_points(mid_plane_pt1, mid_plane_pt2)
		mid_ht_perp = self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert[1]) + \
					  self.dist_of_pt_from_line(mid_plane_m, mid_plane_c, vert[4])
		return ant_ht, mid_ht, mid_ht_perp, post_ht

	def posterior_alignment(self, vert1, vert2):
		above_m, above_c = self.slope_intercept_from_points(vert1[2], vert1[3])
		below_m, below_c = self.slope_intercept_from_points(vert2[2], vert2[3])
		if above_m is None and below_m is None:
			return 0
		if above_m is None:
			return 1 / below_m
		if below_m is None:
			return 1 / above_m
		return (below_m - above_m) / (1 + below_m * above_m)

	def vertebra_slip_ratio(self, vert1, vert2, input_filename, location, save_intermediate_images):
		# antero-posterior line of superior end plate of vertebra below
		superior_m, superior_c = self.slope_intercept_from_points(vert2[2], vert2[0])

		# perpendicular dropped from inferior corner of vertebra above
		perp_pt, superior_pt = (vert1[3], vert2[2]) if location == 'posterior' else (vert1[5], vert2[0])
		intersect_x, intersect_y = self.intersection_with_perpendicular(superior_m, superior_c, perp_pt)

		if save_intermediate_images:
			self.draw_vertebrae_area_ratio_lines((perp_pt[0], perp_pt[1]), (int(intersect_x), int(intersect_y)),
											 (vert2[0][0], vert2[0][1]), (vert2[2][0], vert2[2][1]), input_filename)
		ratio = self.dist_between_pts((intersect_x, intersect_y), superior_pt) / \
				self.dist_between_pts(vert2[0], vert2[2])
		if not self.between_points((intersect_x, intersect_y), vert2[0], vert2[2]):
			ratio *= -1
		return ratio, ratio / self.dist_between_pts((intersect_x, intersect_y), perp_pt)

	def slip_distances(self, vert1, vert2, location):
		above_vert_pt1, above_vert_pt2 = (vert1[2], vert1[3]) if location == 'posterior' else (vert1[0], vert1[5])
		below_vert_pt = vert2[2] if location == 'posterior' else vert2[0]

		# anterior / posterior edge of the vertebra above
		vert_edge_m, vert_edge_c = self.slope_intercept_from_points(above_vert_pt1, above_vert_pt2)

		# line perpendicular to anterior/posterior edge dropped from superior point of vertebra below
		perp_intersect_x, perp_intersect_y = self.intersection_with_perpendicular(vert_edge_m, vert_edge_c, below_vert_pt)

		# antero-posterior line of superior end plate of vertebra below
		superior_m, superior_c = self.slope_intercept_from_points(vert2[2], vert2[0])
		along_intersect_x, along_intersect_y = self.intersection_of_lines(
			superior_m, superior_c, vert_edge_m, vert_edge_c)

		perp_slip = self.dist_between_pts((perp_intersect_x, perp_intersect_y), below_vert_pt)
		along_slip = self.dist_between_pts((along_intersect_x, along_intersect_y), below_vert_pt)

		if not self.between_points((perp_intersect_x, perp_intersect_y), vert2[0], vert2[2]):
			perp_slip *= -1
		if not self.between_points((along_intersect_x, along_intersect_y), vert2[0], vert2[2]):
			along_slip *= -1

		return perp_slip, along_slip

	def slippage_denominator(self, vert1, vert2):
		return self.dist_between_pts(vert1[5], vert1[3]), self.dist_between_pts(vert2[0], vert2[2])





def helper():
	print('\n\n-----------------------------------')
	help(file_operations)
	help(spine_dicomloader)
	print('-----------------------------------\n')

# helper()
